package polytech;

import java.util.Observable;

public class TestPile extends Observable {

	private int size = 0;

	public void test_push(int entier) {
		this.size += 1;
		TestViewBottomPile vbp = new TestViewBottomPile();
		vbp.update((Observable) this, (Object) this.size);
		TestViewTopPile vtp = new TestViewTopPile();
		vtp.update((Observable) this, (Object) this.size);
	};
}