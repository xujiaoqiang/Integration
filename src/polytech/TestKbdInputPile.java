package polytech;

import java.util.Observable;

public class TestKbdInputPile {

	// private TestPile pile = new TestPile();
	private Pile pile = new Pile();

	public void test_actionCommande(int entier) {

		// pile.test_push(entier);
		pile.push(entier);
		TestViewBottomPile vbp = new TestViewBottomPile();
		vbp.update((Observable) pile, (Object) pile.getSizeList());
		TestViewTopPile vtp = new TestViewTopPile();
		vtp.update((Observable) pile, (Object) pile.getSizeList());
	};
}