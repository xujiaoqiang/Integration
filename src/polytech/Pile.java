package polytech;

import java.util.ArrayList;
import java.util.Observable;

public class Pile extends Observable {

	private ArrayList<Integer> list = new ArrayList();

	public Integer getSizeList() {
		return this.list.size();
	}

	public Integer getEntier(int index) {
		return this.list.get(index);
	}

	public void push(int entier) {
		this.list.add(entier);

	}

	public void pop() {
		int size = this.list.size();
		Integer but = this.list.get(size - 1);
		this.list.remove(but);
	}

	public void clear() {
		this.list.clear();
	}
}
